# @Time : 2021/3/11 15:20
# @Author : chao

# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation

def txt_to_df(txt):
    cntVector = CountVectorizer()
    cntTf = cntVector.fit_transform(txt)
    lda = LatentDirichletAllocation(n_components=TOPIC_NUM,
                                    learning_offset=50.,
                                    random_state=0)
    lda.fit_transform(cntTf)
    lda_topics = lda.components_ / lda.components_.sum(axis=1)[:, np.newaxis]
    topics = []
    for topic_idx, topic in enumerate(lda_topics):
        t = []
        ll = list(map(lambda i : (cntVector.get_feature_names()[i], topic[i]), list(topic.argsort()[:-11:-1])))
        for pair in ll:
            t.append(pair[0] + ' ' + str(float('%.8f' % pair[1])))
        topics.append(t)

    df = pd.DataFrame(topics)
    return df

###########################################################
FILE_TXT = r'C:\Users\鲍超\Desktop\小论文——基于在线评论的物流客户满意度研究\代码\数据\预处理后数据\baishi_data.txt'
FILE_WRITE = r'C:\Users\鲍超\Desktop\小论文——基于在线评论的物流客户满意度研究\代码\主题提取\数据\baishi_lda.xlsx'
TOPIC_NUM = 1

with open(FILE_TXT, encoding='ANSI') as f:
    text = f.readlines()

t = txt_to_df(np.array(text))
t.to_excel(FILE_WRITE)
