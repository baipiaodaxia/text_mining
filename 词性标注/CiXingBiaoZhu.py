# @Time : 2021/3/8 20:04
# @Author : chao
#词性标注
import jieba
import jieba.posseg as pseg

jieba.load_userdict(r"C:\Users\鲍超\Desktop\小论文——基于在线评论的物流客户满意度研究\代码\词典\out.txt")

p = open(r"C:\Users\鲍超\Desktop\小论文——基于在线评论的物流客户满意度研究\代码\去除停用词并分词\去除停用词并分词结果\zong_fengci_tingyongci2.txt",
         'r', encoding = 'utf-8')
q = open(r'C:\Users\鲍超\Desktop\小论文——基于在线评论的物流客户满意度研究\代码\词性标注\词性标注\zong_cxbz2.txt',
         'w', encoding = 'utf-8')
for line in p.readlines():
    words = pseg.cut(line)
    for word, flag in words:
        q.write(str(word) + str(flag) + "  ")
    q.write('\n')

